package no.qux.demo.springdemo;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SpringDemoApplicationTests {

    @Test
    void contextLoads() {
        Assertions.assertEquals(0, 0);
    }

}
