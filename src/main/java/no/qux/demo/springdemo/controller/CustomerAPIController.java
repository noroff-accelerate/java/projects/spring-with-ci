package no.qux.demo.springdemo.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import no.qux.demo.springdemo.data.service.CustomerService;
import no.qux.demo.springdemo.model.Customer;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@Tag(name = "Customer")
@RequestMapping("/api")
public class CustomerAPIController {
    private final CustomerService customerService;

    public CustomerAPIController (
            CustomerService customerService
    ) {
        this.customerService = customerService;
    }

    @GetMapping("/customer") // /api/customers
    public Collection<Customer> getAllCustomers(
            @RequestParam(value = "limit", required = false) String limit,
            @RequestParam(value = "offset", defaultValue = "0", required = false) String offset
    ) {
        try {
            return customerService.getAll(Integer.parseInt(limit), Integer.parseInt(offset));
        } catch (NumberFormatException nfe) {
            return customerService.getAll();
        }
    }

    @GetMapping("/customer/{customerId}")
    public Customer getCustomerById (@PathVariable String customerId) {
        return customerService.getById(customerId);
    }

    @PostMapping("/customer")
    public Customer addNewCustomer (@RequestBody Customer customer) {
        return customerService.add(customer);
    }

    @PutMapping("/customer/{customerId}")
    public Customer updateExistingCustomer (@PathVariable String customerId, @RequestBody Customer customer) {
        customer.setCustomerId(customerId);
        return customerService.update(customer);
    }

    @PatchMapping("/customer/{customerId}")
    public Customer updatePartialCustomer (@PathVariable String customerId, @RequestBody Customer customer) {
        customer.setCustomerId(customerId);
        return customerService.partialUpdate(customer);
    }

    @DeleteMapping("/customer/{customerId}")
    public boolean deleteCustomer (@PathVariable String customerId) {
        return customerService.deleteById(customerId);
    }
}
