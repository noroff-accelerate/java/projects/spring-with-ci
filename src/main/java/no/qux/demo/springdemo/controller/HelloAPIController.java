package no.qux.demo.springdemo.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import no.qux.demo.springdemo.model.MessageContainer;
import org.springframework.web.bind.annotation.*;

@RestController()
@RequestMapping("/hello")
@Tag(name = "Hello")
public class HelloAPIController {
    @Operation(summary = "Say Hello!")
    @GetMapping("/")
    public MessageContainer index() {
        MessageContainer f = new MessageContainer();
        f.message = "Hello, World!";
        return f;
    }

    @GetMapping("/greet")
    @Operation(summary = "Say hello to someone special!")
    public MessageContainer greetQuery(
            @RequestParam("name") String name
    ) {
        MessageContainer f = new MessageContainer();
        f.message = String.format("Hello, %s!", name);
        return f;
    }

    @GetMapping("/greet/{name}")
    @Operation(summary = "Say hello to someone special!")
    public MessageContainer greetPathVar(
            @PathVariable("name") String couldBeNamedAnything
    ) {
        MessageContainer f = new MessageContainer();
        f.message = String.format("Hello, %s!", couldBeNamedAnything);
        return f;
    }

    @Operation(summary = "Read a message from the POST body")
    @PostMapping("/upload")
    public String upload(
            @RequestBody MessageContainer data
    ) {
        return data.message;
    }
}
