package no.qux.demo.springdemo.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class MessageContainer {
    @NotBlank
    @Size(min = 0, max = 100)
    public String message;
}
