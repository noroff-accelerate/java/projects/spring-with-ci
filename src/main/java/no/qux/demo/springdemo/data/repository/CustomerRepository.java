package no.qux.demo.springdemo.data.repository;

import no.qux.demo.springdemo.model.Customer;

public interface CustomerRepository extends RestRepository<Customer> {}
