package no.qux.demo.springdemo.data.service;

import no.qux.demo.springdemo.data.repository.CustomerRepository;
import no.qux.demo.springdemo.model.Customer;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class CustomerServiceImpl implements CustomerService {
    private final CustomerRepository customerRepository;

    public CustomerServiceImpl(
            CustomerRepository customerRepository
    ) {
        this.customerRepository = customerRepository;
    }

    @Override
    public Collection<Customer> getAll(int limit, int offset) {
        return customerRepository.getAll(limit, offset);
    }

    @Override
    public Collection<Customer> getAll() {
        return customerRepository.getAll();
    }

    @Override
    public Collection<Customer> searchByCompanyName(String query) {
        if (query == null || query.equals("")) {
            return null; // Should really throw an exception
        }

        return customerRepository.search("CompanyName", query);
    }

    @Override
    public Customer getById(String id) {
        if (id == null || id.equals("")) {
            return null; // Should really throw an exception
        }

        return customerRepository.getById(id);
    }

    @Override
    public Customer add(Customer item) {
        // TODO Validation
        return customerRepository.add(item);
    }

    @Override
    public Customer update(Customer item) {
        if (item.getCustomerId() == null || item.getCustomerId().equals("")) {
            return null; // Should really throw an exception
        }

        int result = customerRepository.update(item);

        if (result > 0) {
            return customerRepository.getById(item.getCustomerId());
        }

        return null; // No update done.
    }

    @Override
    public Customer partialUpdate(Customer item) {
        if (item.getCustomerId() == null || item.getCustomerId().equals("")) {
            return null; // Should really throw an exception
        }

        Customer customer = customerRepository.getById(item.getCustomerId());

        if (customer == null) {
            return null; // Should really throw an exception
        }

        customer.merge(item);
        int result = customerRepository.update(customer);

        if (result > 0) {
            return customer;
        }

        return customerRepository.getById(item.getCustomerId());
    }

    @Override
    public boolean deleteById(String id) {
        return customerRepository.deleteById(id) > 0;
    }
}
