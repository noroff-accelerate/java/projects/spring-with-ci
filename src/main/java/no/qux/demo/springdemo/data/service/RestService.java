package no.qux.demo.springdemo.data.service;

import java.util.Collection;

public interface RestService<T> {
    Collection<T> getAll(int limit, int offset);
    Collection<T> getAll();
    T getById (String id);
    T add (T item);
    T update (T item);
    T partialUpdate (T item);
    boolean deleteById (String id);
}
