package no.qux.demo.springdemo.data.service;

import no.qux.demo.springdemo.model.Customer;

import java.util.Collection;

public interface CustomerService extends RestService<Customer> {
    Collection<Customer> searchByCompanyName(String query);
}
