package no.qux.demo.springdemo.data.repository;

import no.qux.demo.springdemo.data.DatabaseConnectionFactory;
import no.qux.demo.springdemo.model.Customer;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;

@Repository
public class CustomerRepositoryImpl implements CustomerRepository {
    private final DatabaseConnectionFactory connectionFactory;

    public CustomerRepositoryImpl(
            DatabaseConnectionFactory connectionFactory
    ) {
        this.connectionFactory = connectionFactory;
    }

    public Collection<Customer> getAll(int limit, int offset) {
        try (Connection conn = connectionFactory.getConnection()) {
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT Id,CompanyName,ContactName,Phone FROM Customer LIMIT ? OFFSET ?");
            preparedStatement.setInt(1, limit);
            preparedStatement.setInt(2, offset);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                return processResultSet(resultSet);
            }

        } catch (SQLException sqe) {
            sqe.printStackTrace();
            return null;
        }
    }

    @Override
    public Collection<Customer> getAll() {
        try (Connection conn = connectionFactory.getConnection()) {
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT Id,CompanyName,ContactName,Phone FROM Customer");
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                return processResultSet(resultSet);
            }

        } catch (SQLException sqe) {
            sqe.printStackTrace();
            return null;
        }
    }

    @Override
    public Collection<Customer> search(String columnName, String query) {
        try (Connection conn = connectionFactory.getConnection()) {
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT Id,CompanyName,ContactName,Phone FROM Customer WHERE ? LIKE ? COLLATE NOCASE");
            preparedStatement.setString(1, columnName);
            preparedStatement.setString(2, "%" + query + "%");
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                return processResultSet(resultSet);
            }

        } catch (SQLException sqe) {
            sqe.printStackTrace();
            return null;
        }
    }

    private Collection<Customer> processResultSet(ResultSet resultSet) throws SQLException {
        ArrayList<Customer> customers = new ArrayList();

        while(resultSet.next()) {
            Customer customer = new Customer(
                    resultSet.getString("Id"),
                    resultSet.getString("CompanyName"),
                    resultSet.getString("ContactName"),
                    resultSet.getString("Phone")
            );

            customers.add(customer);
        }

        return customers;
    }

    public Customer getById (String id) {
        try (Connection conn = connectionFactory.getConnection()) {
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT * FROM Customer WHERE Id = ?");
            preparedStatement.setString(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            resultSet.next();
            return new Customer(
                    resultSet.getString("Id"),
                    resultSet.getString("CompanyName"),
                    resultSet.getString("ContactName"),
                    resultSet.getString("Phone")
            );

        } catch (SQLException sqe) {
            sqe.printStackTrace();
            return null;
        }
    }

    public Customer add (Customer item) {
        try (Connection conn = connectionFactory.getConnection()) {
            PreparedStatement preparedStatement = conn.prepareStatement("INSERT INTO Customer (CompanyName, ContactName, Phone) VALUES (?, ?, ?)", Statement.RETURN_GENERATED_KEYS);

            preparedStatement.setString(1, item.getCompanyName());
            preparedStatement.setString(2, item.getContactName());
            preparedStatement.setString(3, item.getPhone());

            if (preparedStatement.executeUpdate() > 0) {
                try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        item.setCustomerId(generatedKeys.getString(1));
                    }
                }

                return item;
            }

            return null;
        } catch (SQLException sqe) {
            sqe.printStackTrace();
            return null;
        }
    }

    public int update (Customer item) {
        try (Connection conn = connectionFactory.getConnection()) {
            PreparedStatement preparedStatement = conn.prepareStatement("UPDATE Customer SET CompanyName = ?, ContactName = ?, Phone = ? WHERE Id = ?");

            preparedStatement.setString(1, item.getCompanyName());
            preparedStatement.setString(2, item.getContactName());
            preparedStatement.setString(3, item.getPhone());
            preparedStatement.setString(4, item.getCustomerId());

            return preparedStatement.executeUpdate();

        } catch (SQLException sqe) {
            sqe.printStackTrace();
            return 0;
        }
    }

    public int deleteById (String id) {
        try (Connection conn = connectionFactory.getConnection()) {
            PreparedStatement preparedStatement = conn.prepareStatement("DELETE FROM Customer WHERE Id = ?");

            preparedStatement.setString(1, id);

            return preparedStatement.executeUpdate();

        } catch (SQLException sqe) {
            sqe.printStackTrace();
            return 0;
        }
    }
}
