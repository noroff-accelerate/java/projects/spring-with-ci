package no.qux.demo.springdemo.data.repository;

import java.util.Collection;

public interface RestRepository<T> {
    Collection<T> getAll(int limit, int offset);
    Collection<T> getAll();
    Collection<T> search(String columnName, String query);
    T getById (String id);
    T add (T item);
    int update (T item);
    int deleteById (String id);
}
