package no.qux.demo.springdemo.view;

import no.qux.demo.springdemo.data.service.CustomerService;
import no.qux.demo.springdemo.model.Customer;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.server.ResponseStatusException;

@Controller
public class CustomerController {
    private final CustomerService customerService;

    public CustomerController (
            CustomerService customerService
    ) {
        this.customerService = customerService;
    }

    @GetMapping("/")
    public String view(
            @RequestParam(name = "query", required = false) String query,
            Model model
    ) {
        if (query != null && !query.equals("")) {
            model.addAttribute("customers", customerService.searchByCompanyName(query));
            model.addAttribute("query", query);
        } else {
            model.addAttribute("customers", customerService.getAll());
        }
        return "customerList";
    }

    @GetMapping("/add")
    public String addView(
            Model model
    ) {
        Customer customer = new Customer();
        model.addAttribute("customer", customer);
        return "addCustomer";
    }

    @PostMapping("/add")
    public String addForm(
        @ModelAttribute Customer customer,
        BindingResult errors,
        Model model
    ) {
        if (errors.hasErrors()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        customerService.add(customer);
        return "addCustomer";
    }
}
